package interfete;

import clase.Sponsor;

public interface Pachete {

	public String setMessage();
	public void addToPackage(Sponsor s);
	public void removeFromPackage(int index);
	public Sponsor getSponsor(int index);
	public int getPosition(Sponsor s);
	public int getMoneyPack();
	public int getSize();
	
}
