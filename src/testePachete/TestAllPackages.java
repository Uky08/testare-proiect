package testePachete;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

	@RunWith(Suite.class)
	@SuiteClasses({ testeBasic.class, testeSilver.class, testeGold.class })
	public class TestAllPackages {
		
	}

