package testePachete;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import clase.BasicPack;
import clase.Sponsor;

public class testeBasic {

	@Test
	public void testSetMessage() {
		BasicPack pack=new BasicPack();
		assertEquals("Basic", pack.setMessage());
	}

	@Test
	public void testAddToPackage() {
		BasicPack pack=new BasicPack();
		Sponsor s=new Sponsor("Secu","Cristi","HR Manager","IBM",1600,"EUR","12/03/2015");
		
		pack.addToPackage(s);
		int index=pack.getPosition(s);
		assertEquals(s, pack.getSponsor(index));
			
	}

	@Test
	public void testRemoveFromPackage() {
		BasicPack pack=new BasicPack();
		Sponsor s=new Sponsor("Secu","Cristi","HR Manager","IBM",100,"EUR","12/03/2015");
		Sponsor s1= new Sponsor("Onea", "Simona", "Recruitment advisor", "TotalSoft", 50,"EUR","24/03/2015");

		pack.addToPackage(s);
		pack.addToPackage(s1);
		
		int r=pack.getSize()-1;
		pack.removeFromPackage(1);
		int r1=pack.getSize();
		assertEquals( r, r1);

	}

	@Test
	public void testGetSponsor() {
		BasicPack pack=new BasicPack();
		pack.addToPackage(new Sponsor("Secu","Cristi","HR Manager","IBM",100,"EUR","12/03/2015"));
		pack.addToPackage(new Sponsor("Onea", "Simona", "Recruitment advisor", "TotalSoft", 70,"EUR","24/03/2015"));
		pack.addToPackage(new Sponsor("Secu","Ionela","Sales executive","IBM",40,"EUR","24/05/2015"));
		Sponsor s=new Sponsor("Onea","Ionela","Sales executive","TotalSoft",50,"EUR","25/05/2015");
		pack.addToPackage(s);
		
		Sponsor s1=pack.getSponsor(pack.getPosition(s));
		assertEquals(s,s1);
	}


}
