package testePachete;

import static org.junit.Assert.*;

import org.junit.Test;

import clase.GoldPack;
import clase.SilverPack;
import clase.Sponsor;

public class testeGold {

	@Test
	public void testSetMessage() {
		GoldPack pack=new GoldPack();
		assertEquals("Gold", pack.setMessage());
	}

	@Test
	public void testAddToPackage() {
		GoldPack pack=new GoldPack();
		Sponsor s=new Sponsor("Secu","Cristi","HR Manager","IBM",1800,"EUR","12/03/2015");
		
		pack.addToPackage(s);
		int index=pack.getPosition(s);
		assertEquals(s, pack.getSponsor(index));
			
	}

	@Test
	public void testRemoveFromPackage() {
		GoldPack pack=new GoldPack();
		Sponsor s=new Sponsor("Secu","Cristi","HR Manager","IBM",1500,"EUR","12/03/2015");
		Sponsor s1= new Sponsor("Onea", "Simona", "Recruitment advisor", "TotalSoft", 2500,"EUR","24/03/2015");

		pack.addToPackage(s);
		pack.addToPackage(s1);
		
		int r=pack.getSize()-1;
		pack.removeFromPackage(1);
		int r1=pack.getSize();
		assertEquals( r, r1);

	}

	@Test
	public void testGetSponsor() {
		GoldPack pack=new GoldPack();
		pack.addToPackage(new Sponsor("Secu","Cristi","HR Manager","IBM",1900,"EUR","12/03/2015"));
		pack.addToPackage(new Sponsor("Onea", "Simona", "Recruitment advisor", "TotalSoft", 1700,"EUR","24/03/2015"));
		pack.addToPackage(new Sponsor("Secu","Ionela","Sales executive","IBM",1450,"EUR","24/05/2015"));
		Sponsor s=new Sponsor("Onea","Ionela","Sales executive","TotalSoft",1550,"EUR","25/05/2015");
		pack.addToPackage(s);
		
		Sponsor s1=pack.getSponsor(pack.getPosition(s));
		assertEquals(s,s1);
	}


}
