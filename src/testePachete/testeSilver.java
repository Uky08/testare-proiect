package testePachete;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Test;

import clase.BasicPack;
import clase.SilverPack;
import clase.Sponsor;

public class testeSilver {

	@Test
	public void testSetMessage() {
		SilverPack pack=new SilverPack();
		assertEquals("Silver", pack.setMessage());
	}

	@Test
	public void testAddToPackage() {
		SilverPack pack=new SilverPack();
		Sponsor s=new Sponsor("Secu","Cristi","HR Manager","IBM",800,"EUR","12/03/2015");
		
		pack.addToPackage(s);
		int index=pack.getPosition(s);
		assertEquals(s, pack.getSponsor(index));
			
	}

	@Test
	public void testRemoveFromPackage() {
		SilverPack pack=new SilverPack();
		Sponsor s=new Sponsor("Secu","Cristi","HR Manager","IBM",1000,"EUR","12/03/2015");
		Sponsor s1= new Sponsor("Onea", "Simona", "Recruitment advisor", "TotalSoft", 500,"EUR","24/03/2015");

		pack.addToPackage(s);
		pack.addToPackage(s1);
		
		int r=pack.getSize()-1;
		pack.removeFromPackage(1);
		int r1=pack.getSize();
		assertEquals( r, r1);

	}

	@Test
	public void testGetSponsor() {
		SilverPack pack=new SilverPack();
		pack.addToPackage(new Sponsor("Secu","Cristi","HR Manager","IBM",900,"EUR","12/03/2015"));
		pack.addToPackage(new Sponsor("Onea", "Simona", "Recruitment advisor", "TotalSoft", 700,"EUR","24/03/2015"));
		pack.addToPackage(new Sponsor("Secu","Ionela","Sales executive","IBM",450,"EUR","24/05/2015"));
		Sponsor s=new Sponsor("Onea","Ionela","Sales executive","TotalSoft",550,"EUR","25/05/2015");
		pack.addToPackage(s);
		
		Sponsor s1=pack.getSponsor(pack.getPosition(s));
		assertEquals(s,s1);
	}


}
