package ONG;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;


import clase.ONG;
import clase.PackFactory;
import clase.Sponsor;

public class Validare {

	public static ArrayList<Sponsor> lista =new ArrayList<Sponsor>();
	public static FileReader fr;
    public static BufferedReader in = null;
    
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		
	        fr = new FileReader("C:\\Users\\jeni\\Documents\\facultate\\Anul III\\Semestrul II\\CTS\\Workspace\\CTS Proiect\\ListaSponsori");
	        in = new BufferedReader(fr);
	        String linie=in.readLine();
	        while (linie!= null)  {
	            String[] elemente = linie.split(",");
	            if(elemente.length!=7){
	                throw new Exception("Linie invalida!");
 	            }
	            Sponsor s = new Sponsor();
	            s.setNume(elemente[0]);
	            s.setPrenume(elemente[1]);
	            s.setFunctie(elemente[2]);
	            s.setFirma(elemente[3]);
	            s.setSponsorizare(Integer.parseInt(elemente[4].trim()));
	            s.setMoneda(elemente[5]);
	            s.setData(elemente[6]);
	        
	           
	            linie = in.readLine();
	                            
	           lista.add(s);
	              
	 }
	}
	@AfterClass
	 public static void tearDownAfterClass() throws Exception {
		 fr.close();
		 in.close();
		 
	 }


	@Test
	public void testGetLista() {
		ONG o=new ONG (lista);
		 assertEquals(lista, o.getLista());
		 		 
	}
	@Test
	public void testGetListaNull(){
	ArrayList<Sponsor>list=null;
	
	ONG ong=new ONG(list);
	try{
		System.out.println(ong.getLista());
		fail("Special exception");
		
	}catch (NullPointerException ex){
		System.out.println(" ");
	}
	
	}

	@Test
	public void testGetSponsor() {
		
        Sponsor s=new Sponsor("Cucliciu", "Tanase", "Sales Manager", "IBM", 2000, "EUR", "25/05/2015");		 
		ONG o=new ONG (lista);
		assertEquals(s,o.getSponsor("IBM"));
	}

	@Test
	public void testGetSponsorNull(){
        ONG ong=new ONG(lista);
        assertNull(ong.getSponsor("Google"));
	}


	@Test
	public void testModifySponsor() {
		PackFactory pf=new PackFactory();
		
		Sponsor s= new Sponsor("Secu","Ionela","Sales executive","IBM",400,"EUR","25/05/2015");
		
		DateFormat datef=new SimpleDateFormat("dd/MM/yyyy");
		Date date=new Date();
		
		Sponsor s1= new Sponsor("Secu","Ionela","Sales executive","IBM",2400,"EUR","25/05/2015");
		//s1.setData(datef.format(date).toString());
		
		ONG o=new ONG (lista);
		o.modifySponsor(s);
		int index=lista.indexOf(s1);
		assertEquals(true,o.getLista().contains(s1));
	}

	@Test
	public void testTotalSponsorizari() {
		int suma=0;
		for (Sponsor s : lista) {
			suma=suma+s.getSponsorizare();
		}

		ONG o=new ONG(lista);
		assertEquals(suma, o.totalSponsorizari());
	}


	@Test
	public void testShowMoneyPack() {
		PackFactory pf=new PackFactory();
	int suma=0;
	for (Sponsor s : lista) {
		pf.getPack(s);
		if(s.getSponsorizare()<=1000 && s.getSponsorizare()>300)
			suma=suma+s.getSponsorizare();
	}
		
	    ONG o=new ONG(lista);
		assertEquals(suma, o.showMoneyPack("SILVER"));
	}

}
