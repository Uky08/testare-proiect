package testeSponsor;

import static org.junit.Assert.*;

import org.junit.Test;

import clase.Sponsor;

public class ValidareSponsor {

	@Test
	public void testGetNume() {
		 Sponsor s1= new Sponsor("Onea", "Simona", "Recruitment advisor", "TotalSoft", 750,"EUR","24/03/2015");

		 assertEquals("Onea", s1.getNume());
	}

	@Test
	public void testGetPrenume() {
		Sponsor s1= new Sponsor("Onea", "Simona", "Recruitment advisor", "TotalSoft", 750,"EUR","24/03/2015");

		 assertEquals("Simona", s1.getPrenume());
	}


	@Test
	public void testGetFunctie() {
		Sponsor s1= new Sponsor("Onea", "Simona", "Recruitment advisor", "TotalSoft", 750,"EUR","24/03/2015");

		 assertEquals("Recruitment advisor", s1.getFunctie());
	}


	@Test
	public void testGetFirma() {
		Sponsor s1= new Sponsor("Onea", "Simona", "Recruitment advisor", "TotalSoft", 750,"EUR","24/03/2015");

		 assertEquals("TotalSoft", s1.getFirma());
	}


	@Test
	public void testGetSponsorizare() {
		Sponsor s1= new Sponsor("Onea", "Simona", "Recruitment advisor", "TotalSoft", 750,"EUR","24/03/2015");

		 assertEquals(750, s1.getSponsorizare());
	}


	@Test
	public void testGetMoneda() {
		Sponsor s1= new Sponsor("Onea", "Simona", "Recruitment advisor", "TotalSoft", 750,"EUR","24/03/2015");

		 assertEquals("EUR", s1.getMoneda());
	}


	@Test
	public void testGetData() {
		Sponsor s1= new Sponsor("Onea", "Simona", "Recruitment advisor", "TotalSoft", 750,"EUR","24/03/2015");

		 assertEquals("24/03/2015", s1.getData());
    }

	

	@Test
	public void testToString() {
		Sponsor s1= new Sponsor("Onea", "Simona", "Recruitment advisor", "TotalSoft", 750,"EUR","24/03/2015");

		 assertEquals("TotalSoft a sponsorizat organizatia,la data de 24/03/2015, cu suma 750 prin reprezentantul legal: Onea Simona", s1.toString());
    }

}
