package clase;

public class Originator {
	
	public Memo saveToMemo(Sponsor s) {
		return new Memo(s);
	}

	public Sponsor restoreMemo(Memo m) {
		return m.getSponsor();
	}
	

}
