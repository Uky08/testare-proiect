package clase;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.EmptyStackException;

public class ONG {

	private static ArrayList<Sponsor> lista=new ArrayList<Sponsor>();
	
	private static ONG singleton=null;
	
	Caretaker caretaker=new Caretaker();
	PackFactory pf=new PackFactory();
	
	public static ONG getInstance(){
		if (singleton == null)
		{
			singleton=new ONG();
		}
		return singleton;
	}
	
	private ONG(){}
	

	
	public ONG (ArrayList<Sponsor> listaNoua) {
		if (listaNoua != null) {
			lista = new ArrayList<Sponsor>();
			for (int i = 0; i < listaNoua.size(); i++)
				lista.add(listaNoua.get(i));
		} else
			lista = null;
	}
	
	public ArrayList<Sponsor> getLista() {
		if (lista != null) {
			return lista;
		} else
			throw new NullPointerException();
	}

	public void setLista(ArrayList<Sponsor> listaSponsori){
		if(listaSponsori!=null){
			lista=listaSponsori;
		}
		else
			throw new NullPointerException();
	}
	
	public Sponsor getSponsor(String firma){
        int index = -1;
		
		if (lista != null) {
			if (lista.size() != 0) 
			{
			     	if (firma != null) {
				    for (Sponsor s : lista) {
						if (firma.equalsIgnoreCase(s.getFirma()))
							index=lista.indexOf(s);
						    break;
					     }
					if (index != -1) {
						return lista.get(index);
					} else {
						return null;
					}
				    }
			     	else{
					throw new NullPointerException();
			}
			}
			else {
				throw new EmptyStackException();
			}
		} else
			throw new NullPointerException();

	}
		
	public void setSponsor(Sponsor s)
	{
			int index = -1;
			if (s != null) {
				for (Sponsor sponsor:lista) {
					if (s.getFirma().equalsIgnoreCase(sponsor.getFirma())) {
						index=lista.indexOf(sponsor);
					}
				}
				if (index == -1) {
					lista.add(s);
					pf.getPack(s); //aici
				
				} else	
				{
				System.out.println("Firma exista deja. Se va adauga valoarea contractului la suma deja existenta");	
				this.modifySponsor(s);
				}	
			}

			else
				throw new NullPointerException();

		}
	
public void modifySponsor(Sponsor s){
	int index=-1;
	int suma=0;
	
	if(lista!=null){
		if(lista.size()!=0){
			if (s!=null){
				for (Sponsor sponsor : lista) {
					if(sponsor.getFirma().equalsIgnoreCase(s.getFirma()))
					{
						index=lista.indexOf(sponsor);
						suma=sponsor.getSponsorizare();
						break;
					}
				}
				if (index!=-1)
				{
					Originator o=new Originator();
					Memo m=o.saveToMemo(s);
					caretaker.addMemo(m);
					int sumaf=0;
					sumaf=s.getSponsorizare()+suma;
					
					
					
					Sponsor s1 = new Sponsor(lista.get(index));
					/*Sponsor sponsor = lista.get(index);*/
					s1.setSponsorizare(sumaf);
					DateFormat datef=new SimpleDateFormat("dd/MM/yyyy");
					Date date=new Date();
					s1.setData(datef.format(date).toString());
					s1.setNume(s.getNume());
					s1.setPrenume(s.getPrenume());
					s1.setFunctie(s.getFunctie());
					
					if ((suma<300 && sumaf<300)||(suma<1000 && sumaf<1000)||(suma>1000 && sumaf>1000) )
					{
					pf.modifyPack(s1,index); 
					}
					else
					{
						pf.moveFromPack(s1,s,index);
					}						
					
					lista.remove(index);
					lista.add(s1);
				}
				else System.out.println("Sponsorul nu exista");
			}
			else throw new NullPointerException();
		}
		else throw new EmptyStackException();
	}
	else
		throw new NullPointerException();
}

public int totalSponsorizari(){
	int total=0;
	if (lista!=null){
		if(lista.size()!=0){
			for (Sponsor s : lista) {
				total=total+s.getSponsorizare();
			}
			return total;
		}
		else{
			return 0;
		 }
	}
	else
		throw new NullPointerException();
}

public void allModifiedSponsors() {

	for (int i = 0; i < caretaker.lista_memo.size(); i++) {
		System.out.println(caretaker.getMemo(i).getSponsor());
	}
}

public void showPack(String type){
if (type=="SILVER")
{
	for(int i=0;i<pf.sp.silver.size();i++)
	{
		if (i==0)
			pf.sp.setMessage();
		System.out.println(pf.sp.getSponsor(i));
	}
	}
else
	if (type=="BASIC")
	{
		for(int i=0;i<pf.bp.basic.size();i++)
		{
		if (i==0)
			pf.bp.setMessage();
		System.out.println(pf.bp.getSponsor(i));
		}
	}
	else
		if (type=="GOLD")
	{
		for(int i=0;i<pf.gp.gold.size();i++)
		{
		if (i==0)
			pf.gp.setMessage();
		System.out.println(pf.gp.getSponsor(i));
		}
	}
		else 
			System.out.println("Pachet nedefinit.Reincercati una din optiunile:BASIC,SILVER,GOLD.");
}

public int showMoneyPack(String type){
	if (type=="SILVER")
	{
		return pf.sp.getMoneyPack();
		}
	else
		if (type=="BASIC")
		{
			return pf.bp.getMoneyPack();
		}
		else
			if (type=="GOLD")
		{
			return pf.gp.getMoneyPack();
		}
			else
	return 0;
}


public void citireDinFisier(){
	FileReader fr;
    BufferedReader in = null;
    try {
        fr = new FileReader("C:\\Users\\jeni\\Documents\\facultate\\Anul III\\Semestrul II\\CTS\\Workspace\\CTS Proiect\\ListaSponsori");
        in = new BufferedReader(fr);
        String linie=in.readLine();
        while (linie!= null)  {
            String[] elemente = linie.split(",");
            if(elemente.length!=7){
                throw new Exception("Linie invalida!");
            }
            Sponsor s = new Sponsor();
            s.setNume(elemente[0]);
            s.setPrenume(elemente[1]);
            s.setFunctie(elemente[2]);
            s.setFirma(elemente[3]);
            s.setSponsorizare(Integer.parseInt(elemente[4].trim()));
            s.setMoneda(elemente[5]);
            s.setData(elemente[6]);
        
           
            linie = in.readLine();
                            
           lista.add(s);
        }
    } catch (Exception ex) {
       
    } finally {
        try {
            in.close();
        } catch (Exception ex) {
        }
    }
}
public void afisare() {
	if (lista != null) {
		if (lista.size() != 0) {
			for (Sponsor s : lista) {
				System.out.println(s);
			}

		} else {
			System.out.println("Nu exista sponsori");
		}

	} else
		throw new NullPointerException();

}

}
