package clase;

public class Sponsor {

	private String nume;
	private String prenume;
	private String functie;
	private String firma;
	private int sponsorizare;
	private String moneda;
	private String data;

	public Sponsor(){}
	
	public Sponsor(String nume, String prenume, String functie, String firma,
			int sponsorizare, String moneda,String data) {
		super();
		this.nume = nume;
		this.prenume = prenume;
		this.functie = functie;
		this.firma = firma;
		this.sponsorizare = sponsorizare;
		this.moneda = moneda;
		this.data=data;
	}
	
	public Sponsor(Sponsor sponsor) {
		super();
		this.nume = sponsor.nume;
		this.prenume = sponsor.prenume;
		this.functie = sponsor.functie;
		this.firma = sponsor.firma;
		this.sponsorizare = sponsor.sponsorizare;
		this.moneda = sponsor.moneda;
		this.data=sponsor.data;
	}
	
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	public String getPrenume() {
		return prenume;
	}
	public void setPrenume(String prenume) {
		this.prenume = prenume;
	}
	public String getFunctie() {
		return functie;
	}
	public void setFunctie(String functie) {
		this.functie = functie;
	}
	public String getFirma() {
		return firma;
	}
	public void setFirma(String firma) {
		this.firma = firma;
	}
	public int getSponsorizare() {
		return sponsorizare;
	}
	public void setSponsorizare(int sponsorizare) {
		this.sponsorizare = sponsorizare;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
		
	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return this.firma+" a sponsorizat organizatia,la data de "+this.data+", cu suma "+this.sponsorizare+" prin reprezentantul legal: "+this.nume+" "+this.prenume;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((firma == null) ? 0 : firma.hashCode());
		result = prime * result + ((functie == null) ? 0 : functie.hashCode());
		result = prime * result + ((moneda == null) ? 0 : moneda.hashCode());
		result = prime * result + ((nume == null) ? 0 : nume.hashCode());
		result = prime * result + ((prenume == null) ? 0 : prenume.hashCode());
		result = prime * result + sponsorizare;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sponsor other = (Sponsor) obj;
		if (firma == null) {
			if (other.firma != null)
				return false;
		} else if (!firma.equals(other.firma))
			return false;
		if (functie == null) {
			if (other.functie != null)
				return false;
		} else if (!functie.equals(other.functie))
			return false;
		if (moneda == null) {
			if (other.moneda != null)
				return false;
		} else if (!moneda.equals(other.moneda))
			return false;
		if (nume == null) {
			if (other.nume != null)
				return false;
		} else if (!nume.equals(other.nume))
			return false;
		if (prenume == null) {
			if (other.prenume != null)
				return false;
		} else if (!prenume.equals(other.prenume))
			return false;
		if (sponsorizare != other.sponsorizare)
			return false;
		return true;
	}
	

}
