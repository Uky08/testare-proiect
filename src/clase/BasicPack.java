package clase;

import java.util.ArrayList;

import interfete.Pachete;

public class BasicPack implements Pachete{

	static ArrayList<Sponsor> basic=new ArrayList<Sponsor>();
	
	@Override
	public String setMessage() {
		return "Basic";
		
	}

	@Override
	public void addToPackage(Sponsor s) {
		
	    basic.add(s);
		
	}

	@Override
	public void removeFromPackage(int index) {
		basic.remove(index);
		
	}

public Sponsor getSponsor(int index) {
	    
		return basic.get(index);
	}

@Override
public int getMoneyPack() {
	int sum=0;
	for (Sponsor s : basic) {
		sum=sum+s.getSponsorizare();
	}
	return sum;
}

@Override
public int getPosition(Sponsor s) {
	int index=-1;
	for (Sponsor spon : basic) {
		if (spon.getFirma()==s.getFirma())
		{
			index=basic.indexOf(spon);
		}
		
	}
	return index;
}

@Override
public int getSize() {
	
	return basic.size();
}
}
