package clase;

import interfete.Pachete;

import java.util.ArrayList;

public class GoldPack implements Pachete {

		static ArrayList<Sponsor> gold= new ArrayList<Sponsor>();
		
		@Override
		public String setMessage() {
			return "Gold";
			
		}

		@Override
		public void addToPackage(Sponsor s) {
			
			gold.add(s);
		}

		@Override
		public void removeFromPackage(int index) {
	        gold.remove(index);		
		}

		public Sponsor getSponsor(int index) {
		    
			return gold.get(index);
		}

		@Override
		public int getMoneyPack() {
			int sum=0;
			for (Sponsor s : gold) {
				sum=sum+s.getSponsorizare();
			}
			return sum;
		}
		@Override
		public int getPosition(Sponsor s) {
			int index=-1;
			for (Sponsor spon : gold) {
				if (spon.getFirma()==s.getFirma())
				{
					index=gold.indexOf(spon);
				}
				
			}
			return index;
		}

		@Override
		public int getSize() {
		
			return gold.size();
		}
	}


