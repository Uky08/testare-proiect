package clase;

public class Memo {

private Sponsor sponsor;

public Memo(Sponsor s){
	sponsor=new Sponsor(s.getNume(),s.getPrenume(),s.getFunctie(),s.getFirma(),s.getSponsorizare(),s.getMoneda(),s.getData());
}
public Sponsor getSponsor(){
	return sponsor;
}
}
