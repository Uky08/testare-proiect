package clase;

import interfete.Pachete;

public class PackFactory {

	public static BasicPack bp=new BasicPack();
	public static SilverPack sp=new SilverPack();
	public static GoldPack gp=new GoldPack();
	
	public Pachete getPack(Sponsor s){
		
		if (s.getSponsorizare()<=300)
		{	
			bp.addToPackage(s);
		    return bp;
		}
		else
			if (s.getSponsorizare()>300 && s.getSponsorizare()<=1000)
		{
			sp.addToPackage(s);
			return sp;
		
		}else{
			gp.addToPackage(s);
			return gp;
		}
	}
	
	public Pachete modifyPack(Sponsor s, int index){
		//cand intram aici, intram din doua motive: fie ca exista un sponsor in pachet si trebuie modificat, fie ca un sponsor din alt pachet avanseaza in acesta

		Pachete pack = this.getPack(s);
		/*Pachete pack = this.getPack(s1);
		int index=this.getPack(s1).getPosition(s1);*/
		
		pack.removeFromPackage(index);
		
		//pack.addToPackage(s);
		return pack;
			
	}
	public Pachete moveFromPack(Sponsor s, Sponsor s1,int index)
	{
		
		Pachete pack=this.getPack(s1);
		//Sponsor s1= new Sponsor(pack.getSponsor(index));
		
		pack.removeFromPackage(index);
		pack.removeFromPackage(pack.getSize()-1);
		
		pack=this.getPack(s);
		return pack;
	}
	
	}


