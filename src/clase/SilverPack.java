package clase;

import java.util.ArrayList;

import interfete.Pachete;

public class SilverPack implements Pachete {

	static ArrayList<Sponsor> silver = new ArrayList<Sponsor>();
	
	@Override
	public String setMessage() {
		return "Silver";
		
	}

	@Override
	public void addToPackage(Sponsor s) {
		
		silver.add(s);
	}

	@Override
	public void removeFromPackage(int index) {
        silver.remove(index);		
	}

	@Override
	public Sponsor getSponsor(int index) {
	    
		return silver.get(index);
	}

	@Override
	public int getMoneyPack() {
		int sum=0;
		for (Sponsor s : silver) {
			sum=sum+s.getSponsorizare();
		}
		return sum;
	}
	@Override
	public int getPosition(Sponsor s) {
		int index=-1;
		for (Sponsor spon : silver) {
			if (spon.getFirma()==s.getFirma())
			{
				index=silver.indexOf(spon);
			}
			
		}
		return index;
	}

	@Override
	public int getSize() {
		
		return silver.size();
	}
}
